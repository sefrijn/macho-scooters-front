import { useEffect } from 'react';
import { theme } from '../styles/theme';

const stripUnit = (value: string) => parseInt(value, 10);

const useScreenSizeClass = () => {
  useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;

      // Determine the screen size based on the provided breakpoints
      let className: string[] = [];
      for (const breakpointName of Object.keys(theme.breakpoints)) {
        console.log(breakpointName);
        if (screenWidth >= stripUnit(theme.breakpoints[breakpointName])) {
          className.push('screen-' + breakpointName);
        }
      }

      // Check if classlist already contains the calculated class
      // if (document.body.classList.contains(className)) {
      //   return;
      // }

      // Add the calculated class to the <body> element
      const deleteClasses = Object.keys(theme.breakpoints).map(
        (breakpointName) => 'screen-' + breakpointName
      );
      document.body.classList.remove(...deleteClasses);
      document.body.classList.add(...className);
    };

    // Attach the event listener and call it initially
    window.addEventListener('resize', handleResize);
    handleResize();

    // Clean up the event listener on unmount
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);
};

export default useScreenSizeClass;
