import { useEffect, useState } from 'react';

const useIsTouchDevice = () => {
  const [isTouchDevice, setIsTouchDevice] = useState(false);

  useEffect(() => {
    const checkTouchDevice = () => {
      if ('ontouchstart' in window || navigator.maxTouchPoints > 0) {
        setIsTouchDevice(true);
      } else {
        setIsTouchDevice(false);
      }
    };

    checkTouchDevice();

    // Add event listeners to handle changes in touch capabilities
    window.addEventListener('touchstart', checkTouchDevice);
    window.addEventListener('pointerdown', checkTouchDevice);
    window.addEventListener('resize', checkTouchDevice);
    return () => {
      // Clean up event listeners when the component unmounts
      window.removeEventListener('touchstart', checkTouchDevice);
      window.removeEventListener('pointerdown', checkTouchDevice);
      window.removeEventListener('resize', checkTouchDevice);
    };
  }, []);

  return isTouchDevice;
};

export default useIsTouchDevice;
