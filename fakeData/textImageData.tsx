export const textImageData = [
  {
    block: 'textImage',
    content: {
      title: 'Scooter',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, diam quis aliquam faucibus, nisl nunc aliquet nunc, quis aliquam nunc nunc nec nisi. Sed euismod, diam quis aliquam faucibus, nisl nunc aliquet nunc, quis aliquam nunc nunc nec nisi.',
      image: '/scooter.jpeg',
    },
  },
];
