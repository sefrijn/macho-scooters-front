import { extendTheme, withDefaultColorScheme } from '@chakra-ui/react';
import { Poppins } from 'next/font/google';

const poppins = Poppins({
  weight: ['400', '500', '700'],
  subsets: ['latin'],
  display: 'swap',
});

export const gapBase = { base: 4, sm: 8, md: 12, lg: 20 };
export const gapSmall = { base: 4, sm: 6, md: 8 };

export const theme = extendTheme(
  withDefaultColorScheme({ colorScheme: 'brand' }),

  {
    // Breakpoints
    breakpoints: {
      base: '0px', // 0px
      sm: '480px', // ~480px. em is a relative unit and is dependant on the font size.
      md: '768px', // ~768px
      lg: '992px', // ~992px
      xl: '1280px', // ~1280px
      '2xl': '1536px', // ~1536px
    },

    // Fonts
    fonts: {
      body: `${poppins.style.fontFamily}, sans-serif`,
    },

    // Colors
    colors: {
      brand: {
        light: '#faf7f4',
        'medium-light': '#fcddc9',
        medium: '#ec9136',
        dark: '#fe6601',
      },
      gold: '#ffc449',
    },

    // Default styles
    styles: {
      global: {
        body: {
          color: 'gray.800',
        },
        a: {
          textDecoration: 'none',
          _hover: {
            color: 'brand.dark',
            textDecoration: 'none',
          },
        },
      },
    },

    // Default components
    components: {
      Heading: {
        variants: {
          'section-title': {
            fontSize: '2rem',
            fontWeight: 'bold',
            color: 'brand.medium',
          },
          footer: {
            textTransform: 'uppercase',
            color: 'white',
            fontSize: 'md',
            mb: 3,
            as: 'h3',
          },
        },
      },
      Link: {
        baseStyle: {
          _hover: {
            color: 'brand.dark',
            textDecoration: 'none',
          },
        },
        variants: {
          'white-link': {
            color: 'white',
            _hover: {
              color: 'brand.medium-light',
            },
          },
        },
      },
      Button: {
        baseStyle: {
          bgColor: 'brand.dark',
          color: 'white',
          textTransform: 'uppercase',
          _hover: {
            bgColor: 'brand.medium',
          },
        },
        defaultProps: {
          colorScheme: 'brand',
        },
      },
    },
  }
);
