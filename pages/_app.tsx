import 'node_modules/modern-normalize/modern-normalize.css';
import '../styles/globals.css';
import { ChakraProvider } from '@chakra-ui/react';
import { theme } from '../styles/theme';
import Header from '../components/header';
import Footer from '../components/footer';
import useScreenSizeClass from '../hooks/useBreakpoints';

function MyApp({ Component, pageProps }) {
  useScreenSizeClass();
  return (
    <ChakraProvider theme={theme}>
      <Header />
      <Component {...pageProps} />
      <Footer />
    </ChakraProvider>
  );
}

export default MyApp;
