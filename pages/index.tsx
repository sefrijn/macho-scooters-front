import Head from 'next/head';
import Hero from '../components/hero';
import TextImage from '../components/textImage';
import Benefits from '../components/benefits';
import Testimonials from '../components/testimonials';
import { textImageData } from '../fakeData/textImageData';

export default function Home() {
  return (
    <>
      <Head>
        <title>Macho Scooters</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Hero />
      <Benefits />
      <TextImage data={textImageData[0].content} />
      <TextImage data={textImageData[0].content} />
      <Testimonials />
    </>
  );
}
