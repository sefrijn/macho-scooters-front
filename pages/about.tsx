import React from 'react';
import Head from 'next/head';
import TextImage from '../components/textImage';
import { textImageData } from '../fakeData/textImageData';
import { FAQ } from '../components/FAQ';

export default function About() {
  return (
    <>
      <Head>
        <title>About | Macho Scooters</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <TextImage data={textImageData[0].content} />
      <TextImage data={textImageData[0].content} />
      <FAQ />
    </>
  );
}
