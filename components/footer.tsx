import React from 'react';
import { gapBase, gapSmall } from '../styles/theme';
import {
  Text,
  Flex,
  Grid,
  GridItem,
  SimpleGrid,
  VStack,
  Heading,
} from '@chakra-ui/react';
import Logo from './logo';
import { NavLinks } from './navLinks';
import Link from 'next/link';
import { FaFacebook } from 'react-icons/fa6';
import { Link as ChakraLink } from '@chakra-ui/react';

export default function Footer() {
  return (
    <>
      <Grid
        bg={'brand.medium'}
        gap={gapSmall}
        p={gapBase}
        templateColumns={{ base: 'repeat(6, 1fr)' }}
        justifyItems={'start'}
        color={'white'}
        fontSize={'sm'}
      >
        <GridItem colSpan={2}>
          <Flex flexDir={'column'} gap={2} mt={-5} pr={8}>
            <Logo color={'white'} />
            <Text color={'white'}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore.
            </Text>
          </Flex>
        </GridItem>
        <GridItem>
          <Flex flexDir={'column'} gap={1}>
            <Heading variant={'footer'}>Categories</Heading>
            <NavLinks flexDir={'column'} gap={1} whiteLink fontSize={14} />
          </Flex>
        </GridItem>
        <GridItem>
          <Flex flexDir={'column'} gap={1}>
            <Heading variant={'footer'}>Opening Hours</Heading>
            <Text>Mo-Fr: 9am - 6pm</Text>
            <Text>Sat: 10am - 4pm</Text>
            <Text>Sun: Closed</Text>
          </Flex>
        </GridItem>
        <GridItem>
          <Flex flexDir={'column'} gap={1}>
            <Heading variant={'footer'}>Contact </Heading>
            <Text>Tel: +599-7178980</Text>
            <Text>WhatsApp: +599-786</Text>
            <Text>info@macho.com</Text>
            <Text>Adres, Bonaire</Text>
          </Flex>
        </GridItem>
        <GridItem>
          <Flex flexDir={'column'} gap={1}>
            <Heading variant={'footer'}>Social</Heading>
            <ChakraLink
              fontSize={'2xl'}
              href={'https://www.facebook.com/'}
              target={'_blank'}
              rel={'noopener noreferrer'}
              _hover={{ color: 'brand.medium-light' }}
            >
              <FaFacebook />
            </ChakraLink>
          </Flex>
        </GridItem>
      </Grid>
    </>
  );
}
