import React from 'react';
import {
  Box,
  Text,
  Grid,
  SimpleGrid,
  Heading,
  VStack,
  Flex,
} from '@chakra-ui/react';
import Image from 'next/image';
import { gapBase } from '../styles/theme';

export default function TextImage(props: { data: any }) {
  return (
    <Flex
      direction={{ base: 'column', md: 'row' }}
      _even={{
        flexDirection: {
          base: 'column',
          md: 'row-reverse',
        },
        bg: 'brand.light',
      }}
      gap={gapBase}
      p={gapBase}
      alignItems={'center'}
    >
      <Box
        width={{ base: '100%', md: '50%' }}
        borderRadius={8}
        overflow={'clip'}
        position={'relative'}
      >
        <Image
          src={props.data.image}
          alt={props.data.title}
          sizes="40vw"
          style={{
            width: '100%',
            height: 'auto',
          }}
          width={500}
          height={300}
        />
      </Box>
      <VStack
        width={{ base: '100%', md: '50%' }}
        gap={6}
        alignItems={'start'}
        _even={{
          order: {
            base: '2',
            md: '1',
          },
        }}
      >
        <Heading as={'h2'} variant={'section-title'}>
          {props.data.title}
        </Heading>
        <Text>{props.data.text}</Text>
      </VStack>
    </Flex>
  );
}
