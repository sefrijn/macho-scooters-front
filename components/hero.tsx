import { useState } from 'react';
import { Button, Card, CardBody, Center, Flex, Show } from '@chakra-ui/react';
import Image from 'next/image';
import { StyledDatePicker } from './styledDatePicker';
import { Search2Icon } from '@chakra-ui/icons';

export default function Hero() {
  const [dateStart, setDateStart] = useState(new Date());
  const [dateEnd, setDateEnd] = useState(new Date());

  return (
    <Flex
      height={'60vh'}
      minHeight={[0, 0, '400px']}
      position={'relative'}
      justifyContent={{ base: 'end', md: 'center' }}
      alignItems={'end'}
    >
      <Image
        src={'/scooter.jpeg'}
        alt={'Scooter'}
        width={0}
        height={0}
        sizes={'100vw'}
        style={{
          height: '100%',
          width: '100%',
          objectFit: 'cover',
          position: 'absolute',
          zIndex: -1,
        }}
      />
      <Card mx={4} mb={12}>
        <CardBody>
          <Flex
            gap={5}
            alignItems={{ base: 'center', md: 'end' }}
            flexDir={{ base: 'column', md: 'row' }}
          >
            <StyledDatePicker
              text={'Pickup Date'}
              date={dateStart}
              onDateChange={setDateStart}
            />
            <StyledDatePicker
              text={'Return Date'}
              date={dateEnd}
              onDateChange={setDateEnd}
            />
            <Button leftIcon={<Search2Icon />}>Find my scooter</Button>
          </Flex>
        </CardBody>
      </Card>
    </Flex>
  );
}
