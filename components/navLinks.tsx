import { Flex, ResponsiveValue } from '@chakra-ui/react';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import { Link } from '@chakra-ui/react';

NavLinks.defaultProps = {
  flexDir: { base: 'column', lg: 'row' },
  gap: { base: 8, lg: 8, xl: 12 },
  whiteLink: false,
  fontSize: 20,
};

export function NavLinks(props: {
  gap?: ResponsiveValue<any>;
  flexDir?: ResponsiveValue<any>;
  whiteLink?: boolean;
  fontSize?: ResponsiveValue<any>;
}) {
  return (
    <Flex gap={props.gap} fontSize={props.fontSize} flexDir={props.flexDir}>
      <NavItem href={'/'} text={'Home'} whiteLink={props.whiteLink} />
      <NavItem href={'/about'} text={'About'} whiteLink={props.whiteLink} />
      <NavItem
        href={'/book-scooters'}
        text={'Scooters'}
        whiteLink={props.whiteLink}
      />
      <NavItem href={'/contact'} text={'Contact'} whiteLink={props.whiteLink} />
    </Flex>
  );
}

function NavItem(props: { href: string; text: string; whiteLink: boolean }) {
  const router = useRouter();
  const isActive = router.pathname === props.href;
  return (
    <NextLink href={props.href} passHref legacyBehavior>
      <Link
        variant={props.whiteLink ? 'white-link' : 'default'}
        color={props.whiteLink ? 'white' : isActive ? 'brand.dark' : 'gray.800'}
      >
        {props.text}
      </Link>
    </NextLink>
  );
}
