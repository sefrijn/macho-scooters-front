import React from 'react';
import { Heading, SimpleGrid, Text, VStack } from '@chakra-ui/react';

const data = [
  {
    title: 'Bike hire',
    text: 'We have a wide range of bikes for hire, from city bikes to mountain bikes. We also have a range of accessories available to hire, including helmets, locks, and child seats.',
    link: '/book-scooters',
  },
  {
    title: 'Fun trails',
    text: 'We have a wide range of bikes for hire, from city bikes to mountain bikes. We also have a range of accessories available to hire, including helmets, locks, and child seats.',
  },
  {
    title: 'Ready to go',
    text: 'We have a wide range of bikes for hire, from city bikes to mountain bikes. We also have a range of accessories available to hire, including helmets, locks, and child seats.',
  },
];

export default function Benefits() {
  return (
    <SimpleGrid mx={20} my={28} gap={16} columns={{ base: 1, lg: 3 }}>
      {data.map((item, index) => {
        return (
          <VStack key={index} gap={5} color={'gray.500'}>
            <Heading as="h2" size="xl">
              {item.title}
            </Heading>
            <Text textAlign={'center'}>{item.text}</Text>
          </VStack>
        );
      })}
    </SimpleGrid>
  );
}
