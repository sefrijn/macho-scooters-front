import { useDisclosure } from '@chakra-ui/hooks';
import {
  Box,
  Button,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  Text,
  Input,
  Flex,
} from '@chakra-ui/react';
import { useRef } from 'react';
import { NavLinks } from './navLinks';
import { Cross as Hamburger } from 'hamburger-react';
import Image from 'next/image';
import Link from 'next/link';

export function NavMobile() {
  const { isOpen, onOpen, onClose, onToggle } = useDisclosure();
  const btnRef = useRef();

  return (
    <>
      <Button
        _hover={{ bgColor: 'transparent' }}
        variant={'ghost'}
        ref={btnRef}
        onClick={onOpen}
      >
        <Hamburger toggled={isOpen} toggle={onToggle} />
      </Button>
      <Drawer
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        finalFocusRef={btnRef}
      >
        <DrawerOverlay />
        <DrawerContent bgColor={'orange.50'}>
          <DrawerHeader>
            <Link href={'/'} style={{ lineHeight: 0 }}>
              <Image
                className={'logo'}
                src={'/logo.svg'}
                alt={'Logo'}
                width={0}
                height={0}
              />
            </Link>
          </DrawerHeader>
          <DrawerBody
            display={'flex'}
            alignItems={'center'}
            justifyContent={'start'}
          >
            <NavLinks />
          </DrawerBody>
          <DrawerFooter justifyContent={'start'}>
            <Link href={'tel:+31612345678'}>
              <Flex gap={5} alignItems={'center'}>
                <Image
                  src={'/call_now.svg'}
                  alt={'call now'}
                  width={40}
                  height={40}
                />
                <Text>Call us</Text>
              </Flex>
            </Link>
          </DrawerFooter>
        </DrawerContent>
      </Drawer>
    </>
  );
}
