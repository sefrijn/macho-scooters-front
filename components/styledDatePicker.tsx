import { Stack, Text } from '@chakra-ui/react';
import { SingleDatepicker } from 'chakra-dayzed-datepicker';

export function StyledDatePicker(props: {
  text: string;
  date: Date;
  onDateChange: (value: ((prevState: Date) => Date) | Date) => void;
}) {
  const styles = {
    dateNavBtnProps: {
      fontWeight: 'normal',
    },
    dayOfMonthBtnProps: {
      defaultBtnProps: {
        _hover: {
          background: 'brand.dark',
          color: 'white',
        },
        _pressed: {
          background: 'orange.100',
        },
      },
    },
    weekdayLabelProps: {
      fontWeight: 'normal',
    },
    dateHeadingProps: {
      fontWeight: 'normal',
    },
  };

  // TODO: Mobile styles don't fit screen yet

  return (
    <Stack>
      <Text
        fontSize={'sm'}
        textTransform={'uppercase'}
        fontWeight={'semibold'}
        color={'gray.400'}
      >
        {props.text}
      </Text>
      <SingleDatepicker
        name="date-input"
        date={props.date}
        onDateChange={props.onDateChange}
        propsConfigs={styles}
        minDate={new Date()}
      />
    </Stack>
  );
}
