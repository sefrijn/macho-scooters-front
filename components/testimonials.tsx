import React from 'react';
import {
  Card,
  Grid,
  Heading,
  HStack,
  SimpleGrid,
  Text,
  VStack,
  Icon,
} from '@chakra-ui/react';
import { gapBase, gapSmall } from '../styles/theme';
import { FaStar, FaStarHalfAlt, FaRegStar } from 'react-icons/fa';

const data = [
  {
    date: 'jan 2023',
    name: 'Maud Stokes',
    text: 'I love renting this scooter! It is so much fun to ride and it is very easy to use.',
    rating: 4.3,
  },
  {
    date: 'jan 2023',
    name: 'Paul Simon',
    text: 'I love renting this scooter! It is so much fun to ride and it is very easy to use.',
    rating: 5,
  },
  {
    date: 'jan 2023',
    name: 'Maud Stokes',
    text: 'I love renting this scooter! It is so much fun to ride and it is very easy to use.',
    rating: 4,
  },
];

function Ratings({ rating }: { rating: number }) {
  const stars = [];
  for (let i = 1; i <= 5; i++) {
    if (i <= rating) {
      stars.push(<Icon as={FaStar} key={i} />);
    } else if (i - rating < 1) {
      stars.push(<Icon as={FaStarHalfAlt} key={i} />);
    } else {
      stars.push(<Icon as={FaRegStar} key={i} />);
    }
  }
  return (
    <HStack color={'gold'} fontSize={'1.3rem'}>
      {stars}
    </HStack>
  );
}

export default function Testimonials() {
  return (
    <VStack bg={'brand.light'} p={gapBase} gap={8}>
      <Heading as={'h2'} variant={'section-title'}>
        Costumer reviews
      </Heading>
      <SimpleGrid width={'100%'} gap={gapSmall} columns={{ base: 1, lg: 3 }}>
        {data.map((item, index) => {
          return (
            <Card p={6} key={index} gap={6}>
              <VStack align={'start'}>
                <Text>{item.date}</Text>
                <Heading as="h3" size="md">
                  {item.name}
                </Heading>
              </VStack>
              <Text>{item.text}</Text>
              <Ratings rating={item.rating} />
            </Card>
          );
        })}
      </SimpleGrid>
    </VStack>
  );
}
