import { Box, Button, Flex, Hide, Show } from '@chakra-ui/react';
import Link from 'next/link';
import Image from 'next/image';
import { NavMobile } from './navMobile';
import { NavLinks } from './navLinks';
import { gapBase, theme } from '../styles/theme';
import Logo from './logo';

export default function Header() {
  return (
    <Flex
      px={gapBase}
      py={4}
      gap={{ base: 2, lg: 8, xl: 12 }}
      alignItems={'center'}
      justifyContent={'space-between'}
      position={'sticky'}
      top={0}
      bg={'white'}
      zIndex={50}
      boxShadow={'base'}
    >
      <Link href={'/'} style={{ lineHeight: 0 }}>
        <Logo color={theme.colors.brand.dark} width={{ base: 24, md: 32 }} />
      </Link>
      <Show above="lg">
        <Box mr={'auto'}>
          <NavLinks />
        </Box>
      </Show>

      <Show above="lg">
        <Link href={'tel:+31612345678'}>
          <Image
            src={'/call_now.svg'}
            alt={'call now'}
            width={40}
            height={40}
          />
        </Link>
        <Flex gap={5}>
          <Link href={'/nl'}>NL</Link>
          <Link href={'/en'}>EN</Link>
        </Flex>
      </Show>
      <Box ml={{ base: 'auto', lg: 0 }}>
        <Link href={'/book-scooters'}>
          <Button size={['sm', 'md']}>Book</Button>
        </Link>
      </Box>
      <Hide above="lg">
        <NavMobile />
      </Hide>
    </Flex>
  );
}
